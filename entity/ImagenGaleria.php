<?php

class ImagenGaleria
{
  
    private $nombre;
    private $descripcion;

    const RUTA_IMAGENES_PORTFOLIO ="assets/img/portfolio/";


    public function __construct(string $nombre = "", string $descripcion = "" )

    {


        $this->nombre = $nombre;

        $this->descripcion = $descripcion;

     

    }

    public function getURLPortfolio() : string

    {

        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();

    }

   

    public function getId(){ return $this->id;}
    public function getNombre(){ return $this->nombre;}
    public function getDescripcion(){ return $this->descripcion;}

    
    


}
