

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="assets/img/navbar-logo.svg"
                    alt="" /></a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars ml-1"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ml-auto">
                    <li class="<?php if(Menu("index")) echo "active"; ?> lien"><a href="<?php if(Menu("index")) echo "#"; else echo "index.php";?>">Home |</a></li><br>
                    <li class="<?php if(Menu("services")) echo "active"; ?> lien"><a href="<?php if(Menu("services")) echo "#"; else echo "#services";?>">| Services | </a></li> 
                    <li class="<?php if(Menu("Portfolio")) echo "active"; ?> lien"><a href="<?php if(Menu("Portfolio")) echo "#"; else echo "portfolio.php";?>">| Portfolio | </a></li>  
                    <li class="<?php if(Menu("about")) echo "active"; ?> lien"><a href="<?php if(Menu("about")) echo "#"; else echo "#about";?>">| About | </a></li>
                    <li class="<?php if(Menu("Team")) echo "active"; ?> lien"><a href="<?php if(Menu("Team")) echo "#"; else echo "#team";?>"> | Team  |</a></li> 
                    <li class="<?php if(Menu("contact")) echo "active"; ?> lien"><a href="<?php if(Menu("contact")) echo "#"; else echo "#contact";?>"> | Contact |</a></li>  
                    <li class="<?php if(Menu("galeria")) echo "active"; ?> lien"><a href="<?php if(Menu("galeria")) echo "#"; else echo "galeria.php";?>"> | Galeria</a></li>            
                </ul>
            </div>
        </div>
    </nav>


