<?php

require_once ("utils/utils.php"); 
require_once "entity/ImagenGaleria.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";

//conecta la base
$connection = DBConnect::make();
$queryBuilder = new QueryBuilder($connection);

//Convirtiendo registros en objetos
$imagenes = $queryBuilder->findAll("portfolio", "ImagenGaleria");

require_once "views/portfolio.view.php";



?>