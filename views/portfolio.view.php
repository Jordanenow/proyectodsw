<?php include __DIR__ . "/partials/inicio-doc.part.php"; ?>


<?php include __DIR__ . "/partials/nav.part.php"; ?>


<!-- Portfolio Grid-->

<section class="page-section bg-light" id="portfolio">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Portfolio</h2>
                <h3 class="section-subheading text-muted">Tu expositor personal.</h3>
            </div>
            <div class="row">
            <?php 
            
            //Mostramos las imagenes(los objetos)
            for ($i = 0; $i < count($imagenes); $i++){
          include __DIR__ . "/partials/fotitos.part.php"; 
            }
            ?>
            </div>
        </div>
    </section>

    <?php include __DIR__ . "/partials/fin-doc.part.php"; ?>