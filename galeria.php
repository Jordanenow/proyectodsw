<?php

require_once "utils/utils.php";
require_once "utils/File.php";
require_once "exceptions/FileException.php";
require_once "entity/ImagenGaleria.php";
require_once "database/Connection.php";

//echo $_SERVER["PHP_SELF"];

try {
    $connection = DBConnect::make();
    

    if ($_SERVER["REQUEST_METHOD"]==="POST") {  
 
 
        try{
        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

        

        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $imagen = new File("imagen", $tiposAceptados);     
       
       
           //  Funcion que subi el archivo/ Y la ruta donde se subira
        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_PORTFOLIO); 
      
        //$imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_PORTFOLIO, ImagenGaleria::RUTA_IMAGENES_GALLERY);
        
        $mensaje = "Enviado";

        //la sentencia sql para subir la imagen
        $sql = "INSERT INTO portfolio (nombre, descripcion) VALUES (:nombre, :descripcion)";
        // prepare( evitar inyecciones sql y prepara la sentencia)
        $pdoStatement = $connection->prepare($sql);
        $pdoStatement->execute(array(
            ":nombre"=> $imagen->getFileName(),
            ":descripcion"=>$descripcion
        ));

  

    } catch (FileException $fileException) {

        $errores [] = $fileException->getMessage();

    }  
}
}
    catch (QueryException $queryException) {

        $errores [] = $queryException->getMessage();
    
    }   
    
    


require_once "views/galeria.view.php";

?>