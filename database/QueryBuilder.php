<?php
require_once "exceptions/QueryException.php";

class QueryBuilder 

{

    private $connection;
   
    public function __construct(PDO $connection)
   
    {
   
    $this->connection = $connection;
   
    }
   
    public function findAll(string $table, string $classEntity) 
   
    {
   
    $sql = "SELECT * FROM $table";
   
    $pdoStatement = $this->connection->prepare($sql);
   
    if ($pdoStatement->execute()===false)
   
        throw new QueryException("No se ha podido ejecutar la consulta", 1);
   
    return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $classEntity); 
    
    } 
   
   }
   
?>   
